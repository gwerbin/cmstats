import numpy
from setuptools import setup, Extension
from Cython.Build import cythonize

setup(
    name='cmstats',
    version='1.0.0',
    ext_modules=cythonize('cmstats.pyx'),
    include_dirs=[numpy.get_include()],
    install_requires=['numpy>=1.15'],
    zip_safe=False
)
