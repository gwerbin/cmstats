# CMStats

Fast `O(N)` calculation of confusion matrix statistics for multilabel classification.

## Installation

```bash
pip install cython
pip install 'git+https://gitlab.com/gwerbin/cmstats#egg=cmstats'
```

## Usage

See `test.py` for usage example

