#cython: language_level=3

import cython
import numpy as np
cimport numpy as np
from collections import defaultdict


class LabelMetrics:
    __repr_template = 'LabelMetrics({0:d}, {1:d}, {2:d})'
    __str_template = 'Observed:  {0:d}\nPredicted: {1:d}\nCorrect:   {2:d}'


    def __init__(self, int labeled=0, int correct=0, int predicted=0):
        self.labeled = labeled
        self.predicted = predicted
        self.correct = correct

    def todict(self):
        return {
            'labeled': self.labeled,
            'predicted': self.predicted,
            'correct': self.correct
        }

    def __repr__(self):
        return self.__repr_template.format(self.labeled, self.correct, self.predicted)

    def __str__(self):
        return self.__str_template.format(self.labeled, self.correct, self.predicted)


# See: training/benchmark_precision-recall-f1.ipynb

@cython.wraparound(False)
@cython.boundscheck(False)
def cmstats(np.ndarray labels, np.ndarray predictions):
    cdef int n_correct = 0
    cdef int n_labels = 0
    cdef int n_predictions = 0
    cdef int n_intersect = 0
    cdef float precision, recall, f1
    cdef Py_ssize_t i
    cdef Py_ssize_t n = len(labels)
    cdef list ys, ps
    cdef str p

    label_metrics = defaultdict(LabelMetrics)

    for i in range(n):
        ys = labels[i]
        n_labels += len(ys)

        ps = predictions[i]
        n_predictions += len(ps)

        for y in ys:
            label_metrics[y].labeled += 1

        for p in ps:
            if p in ys:
                n_intersect += 1
                break

        for p in ps:
            label_metrics[p].predicted += 1
            if p in ys:
                n_correct += 1
                label_metrics[p].correct += 1

    return (n_correct, n_intersect, n_predictions, n_labels), dict(label_metrics)
