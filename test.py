from pprint import pprint
import numpy as np
from cmstats import cmstats

def test():
    y_true = np.asarray([
        ['cat', 'flower', 'banana'],
        ['flower', 'turtle'],
        ['cat', 'turtle'],
        ['banana', 'turtle']
    ])

    y_pred = np.asarray([
        ['cat', 'banana'],
        ['banana', 'turtle'],
        ['cat', 'flower', 'turtle'],
        ['banana', 'turtle']
    ])

    metrics_microavg, metrics_labelwise = cmstats(y_true, y_pred)

    assert metrics_microavg == (7, 4, 9, 9)
    assert {k: v.todict() for k, v in metrics_labelwise.items()} == {
        'cat':    {'labeled': 2, 'predicted': 2, 'correct': 2},
        'flower': {'labeled': 2, 'predicted': 1, 'correct': 0},
        'banana': {'labeled': 2, 'predicted': 3, 'correct': 2},
        'turtle': {'labeled': 3, 'predicted': 3, 'correct': 3}
    }


if __name__ == '__main__':
    test()
